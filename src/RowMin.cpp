#include <Rcpp.h>

using namespace Rcpp;

//' Find the minimum value for each row of a matrix
//' 
//' @param x A matrix containing values of type numeric
//' @export
//' @author Chaitanya Acharya	(Duke University)
// [[Rcpp::export]]
NumericVector RowMin(NumericMatrix x){
  NumericVector out(x.nrow());
  for (int i = 0; i < x.nrow(); i++){
    out[i] = min(x(i,_));
  }
  return wrap(out);
}
