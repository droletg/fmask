% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/calculateSimilarity.R
\name{calculateSimilarity}
\alias{calculateSimilarity}
\title{Similarity}
\usage{
calculateSimilarity(x, y)
}
\arguments{
\item{x}{Data table containing columns:
\itemize{
  \item{labels: }{original cloud objects IDs}
  \item{o.cell: }{original cloud objects cell numbers}
  \item{s.cell: }{projected shadow cell numbers}
}}

\item{y}{Data table containing a single column:
\itemize{
  \item{cell: }{cell numbers of cells in the combined potential cloud layer-
  cloud shadows-out of image cells  (i.e. no-data values surrounding the 
  image)}
}}
}
\value{
Data table of similarity values for each projected shadow object in
the image
}
\description{
Calculates similarity between projected shadows and potential 
cloud and cloud shadow layers in a Landsat scene.
}
\details{
For each shadow object, the similarity value is the ratio of the 
overlap area between the calculated shadow and the potential cloud or shadow 
layers to the calculated shadow area, and is calculated as:

s = a / b, where:

a = number of overlapping pixels between b and combined po-
    tential cloud and potential cloud shadow layers plus those out-
    side of the image boundary
b = number of shadow pixels not in its corresponding cloud object 
    plus those outside of the image boundary
}
\author{
Guillaume Drolet
}
\keyword{internal}

