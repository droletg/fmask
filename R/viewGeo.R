#' Sensor view geometry
#' 
#' Calculates view geometry parameters for a Landsat scene. These parameters are
#' used to correct pixel locations for view geometry effects in the Fmask 
#' algorithm.
#' 
#' @param x RasterLayer of a complete (i.e. not cropped) Landsat scene with 
#' values inside the boundary area of the image equal to 0. This object is 
#' returned by findNonMissingPixels()
#' @return Data frame containing the parameters of the view geometry
#' @note It is very important that x be a complete scene (not a smaller area of
#' the full scene, even if the image to be processed by fmask will be cropped)
#' @references \url{http://code.google.com/p/fmask/}. Code adapted from USGS.
#' @seealso \code{\link{findNonMissingPixels}}
#' @keywords internal
#' @author Guillaume Drolet  
viewGeo <- function(x) {  
  
  # x could be aggregated before running the code to increase speed?
  e.rc <- as.matrix(x)
  e.rc <- which(!e.rc, arr.ind = TRUE)
  
  # x = cols, y = rows
  y.ul <- min(e.rc[, 1])          
  x.ul <- min(e.rc[which(e.rc[, 1] == y.ul), 2])
  y.lr <- max(e.rc[, 1]) 
  x.lr <- max(e.rc[which(e.rc[, 1] == y.lr), 2])
  x.ur <- max(e.rc[, 2])                       
  y.ur <- min(e.rc[which(e.rc[, 2] == x.ur), 1])          
  x.ll <- min(e.rc[, 2])                                          
  y.ll <- min(e.rc[which(e.rc[, 2] == x.ll), 1])         
  
  x.u = (x.ul + x.ur) / 2
  x.l = (x.ll + x.lr) / 2
  y.u = (y.ul + y.ur) / 2
  y.l = (y.ll + y.lr) / 2
  
  k.ulr = (y.ul - y.ur) / (x.ul - x.ur) # get k of the upper left and right points
  k.llr = (y.ll - y.lr) / (x.ll - x.lr) # get k of the lower left and right points
  k.aver = (k.ulr + k.llr) / 2
  omega.par = atan(k.aver) # get the angle of parallel lines k (in pi)
  
  # AX(j) + BY(i) + C = 0
  A = y.u - y.l
  B = x.l - x.u
  C = y.l * x.u - x.l * y.u
  
  omega.per = atan(B / A) # get the angle which is perpendicular to the trace line
  vg <- as.data.frame(cbind(A, B, C, omega.par, omega.per), 
                      row.names = "values")
  
  return(vg)          
}