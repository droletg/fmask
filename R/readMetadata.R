#' Metadata
#' 
#' Reads image metadata from a file into a character vector.
#' 
#' @param f Character. Filename to a valid metadata file  
#' @param na.strings a vector of strings which are to be interpreted as NA 
#' values. Blank fields are also considered to be missing values in logical, 
#' integer, numeric or complex vectors. See \code{\link[utils]{type.convert}} 
#' for details.
#' @return List of parameters and values
#' @keywords internal
#' @author Guillaume Drolet, Etienne B. Racine
#  version 1.0
readMetadata <- function(f, na.strings = "NONE") {
  m <- scan(f, what = "character", sep = "\t", quiet = TRUE, 
            strip.white = TRUE, blank.lines.skip = TRUE)
  m <- m[-length(m)]
  lm <- strsplit(m, " = ")
  k <- lapply(lm, function(x) x[[1]])
  v <- lapply(lm, function(x) x[[2]]) 
  
  k <- lapply(k, function(x) gsub("BAND_", "BAND", x))
  k <- lapply(k, function(x) gsub("QUANTIZE_CAL_MIN", "QCALMIN", x))
  k <- lapply(k, function(x) gsub("QUANTIZE_CAL_MAX", "QCALMAX", x))
  k <- lapply(k, function(x) gsub("RADIANCE_MINIMUM", "LMIN", x))
  k <- lapply(k, function(x) gsub("RADIANCE_MAXIMUM", "LMAX", x))
  k <- lapply(k, function(x) gsub("METADATA_L1_FILE_NAME", "METADATA_FILE_NAME", x))
  v <- lapply(v, function(x) type.convert(gsub("LANDSAT_", "Landsat", x), 
                                          as.is = TRUE, na.strings = na.strings)) 
  names(v) = k
  
  return(v)
}
