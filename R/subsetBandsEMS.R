#' Subset image bands
#' 
#' Creates a subset of image bands for a given platform/sensor combination, with 
#' the proper bands names expected by the \code{\link{fmask}} function.
#' 
#' @param s List of scene acquisition parameters from sceneParams()
#' @param i List of bands parameters from bandsParams()
#' @return A list of subsetted bands parameters for the platform/sensor 
#' combination in s and renamed with corresponding regions of the EMS
#' @seealso \code{\link{sceneParams}}, \code{\link{bandsParams}}
#' @keywords internal
#' @author Guillaume Drolet
#  version 1.0
subsetBandsEMS <- function(s, i) {
  
  sp <- paste(s$SPACECRAFT_ID, s$SENSOR_ID, sep = "-")
  
  switch(sp,
         "Landsat7-ETM" = {
           bands <- c("BAND1","BAND2", "BAND3", "BAND4", "BAND5", "BAND61", 
                      "BAND6_VCID_1", "BAND7")
         },
         "Landsat5-TM" = {
           bands <- c("BAND1","BAND2", "BAND3", "BAND4", "BAND5", "BAND6", "BAND7")
         },
         "Landsat8-OLI_TIRS" = {
           bands <- c("BAND2","BAND3", "BAND4", "BAND5", "BAND6", "BAND11", "BAND7")
         }
  ) 
  b <- i[which(names(i) %in% bands)]
  names(b) <- c("B", "G", "R", "NIR", "SWIR1", "TIR", "SWIR2")
  return(b)
}