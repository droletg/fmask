# fmask

The R package **fmask** is a tool for automatic masking of clouds, cloud shadows 
and snow for Landsat TM/ETM+ images. It is an R implementation of [Fmask](https://code.google.com/p/fmask/) 
(Function of mask), an algorithm for cloud, cloud shadow and snow detection in 
Landsat imagery developed by Zhe Zhu (zhuzhe@bu.edu) and Curtis E. Woodcock 
(curtis@bu.edu) at the Center for Remote Sensing, Department of Geography and Environment, Boston University.


## Motivation

In the context of a research project on climate change and forests, we had to find a way to quickly identify clouds in stacks of Landsat images. I then found this excellent [paper](http://dx.doi.org/10.1016/j.rse.2011.10.028) by Zhu and Woodcock (2012) in which showed an algorithm to identify clouds, cloud shadows and snow pixels. I was very impressed by their results and I was also excited to discover that they provided a compiled Matlab version of their code. When I hit the Download section of the Fmask project, not only I found the Matlab files I was looking for but I also discovered the existence of an R implementation of their code, [fmask-for-r](http://code.google.com/p/fmask-for-r/), developed and maintained by [Joseph Henry](joseph.henry@sydney.edu.au) and collaborators. Since a lot of my work is done with R, I contacted Joseph, who kindly offered to contribute to their project. At the time I thought the fmask-for-r code could really benefit from using recent R packages like **raster** and **data.table** and also from an optimization of how the memory was used in the code. In fact, it was (and still is in some cases) difficult to process a complete Landsat scene in a decent amount of time.


## News

***2016-05-12***

Compiled versions of the code from the Downloads section ***should not be used*** until I release an update with fixes. I'm currently working on it so keep posted.

***2016-01-06***

Release of version 0.1.5: fixes a bug with a non-uniform metadata tag, which was causing problems with filename when using a cropping extent.

***2015-12-16***

Release of version 0.1.4 fixes a bug with package JAGUAR. New version is available from the Downloads section. Optimization of the code continues but work is slow due to other commitments.  

***2015-06-23***

A few weeks back I undertook to improve the speed execution and memory usage of fmask. I do it piece by piece but I think the effort will pay back. For example,
*findNonMissingPixels* now takes about **10X** less time to execute than in its original version. This is really encouraging and gives me the motivation to keep improving the code. Stay tuned for improved versions coming over the next months.    


## Notes

You will find that running the code on a full Landsat scene takes a fair amount of time to complete (hours). If possible, you should try to work with subsets that fit into memory: calculations will be MUCH faster. This is due to disk I/O when working with large rasters. Or, feel free to play with the rasterOptions() before running fmask. Especially, maxmemory and chunksize: increasing these values, if you have the ressources, can help increase code execution speed.


At the moment, if you want to use fmask with ```nodes > 1```,  you first need to set a raster temporary directory name  (e.g.```"mydir"```) with:

```
rasterOptions(tmpdir = "mydir")
```

This is a temporary workaround and should be fixed soon.


## Installation

### Windows
1. Get the zip file from [here](https://bitbucket.org/droletg/fmask/downloads/fmask_0.1.4.zip) 
2. Install the package from the zip:  
In Rgui: Packages -> Install package(s) from local zip...  
From Rterm: install.packages('fmask_0.1.4.zip',repos=NULL)

### Linux
1. Get the zip file from [here](https://bitbucket.org/droletg/fmask/downloads/fmask_0.1.4.tar.gz)
2. Install the package from R:  
install.packages('fmask_0.1.4.tar.gz',repos=NULL)


## Usage

```s
library(fmask)
?fmask
fmask("Landsat_metadata_file.txt", filename = "fmask.tif") # will run on a single CPU
```


## License

This package is free and open source software, licensed under GPL.